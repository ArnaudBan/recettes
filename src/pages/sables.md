---
title: Sablés
mealType: Gouter
layout: ../layouts/MainLayout.astro
---
## Ingrédiens

- 125g de sucre
- 125g de beurre
- 250g de farine
- 1 oeuf
- 1⁄4 c.à.c de sel

## Recette

Mélangez le tout à la main jusqu'à obtenir une pâte qui ne colle pas.

Aplatir la pâte avec le rouleau a patisserie. Faire les sablés avec les emportes piéces

Cuire environ 15 min à four moyen (180°C - thermostat 6). Estimer la cuisson d'après la couleur des sablés.

https://www.marmiton.org/recettes/recette_sables_12210.aspx
