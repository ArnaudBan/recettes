---
title: Semoule sucrée
mealType: Dessert
layout: ../layouts/MainLayout.astro
---
## Ingrédiens

- 500g de semoule fine
- 100g de sucre
- 400ml de lait
- 1 sachet de sucre vanillé

## Recette

Faire chauffer le lait avec le sucre et le sucre vanillé. Attention à ne pas faire déborder le lait.

Quand le lait est chaud, ajouter la semoule en remuant.

Continuer de remuer 5 minute à feu doux, verser dans un saladier et attendre que ça refroidisse
