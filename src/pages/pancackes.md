---
title: Pancacke
mealType: Ptit dej
layout: ../layouts/MainLayout.astro
---
## Ingrédiens

- 250g de farine
- 30g de sucre
- 1 sachet de levure
- 1 pincé de sel
- 2 oeufs
- 65g de beurre fondu
- 300ml de lait

## Recette

Mélanger la farine, le sucre, la levure et le sel

Incorporer  les oeufs et le lait

Faire fondre le beurre et l’incorporer

Laisser reposer une nuit au frigo
