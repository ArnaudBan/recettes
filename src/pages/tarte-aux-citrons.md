---
title: Tarte au citron
mealType: Dessert
layout: ../layouts/MainLayout.astro
---

## Ingrédiens

### Pates

- 250g de farine
- 100g de beurre
- 1 oeuf
- 70g de sucre

### Tarte

- 3 citrons
- 6 cs de sucre
- 2 oeufs
- 125g de beurre

### Meuringue

- 3 blanc d’oeufs
- 100g de sucre
- 1 picé de sel

## Recettes

### Pates

Préparer la pâte et garnir un moule à tarte beurré. Piquer le fond et faire cuire à blanc 10 min à 180°C (Th 6).

### Tarte

Presser les citrons dans une casserole et ajouter les zestes

Ajouter le sucre et les jaunes oeufs ( on garde les blancs pour la merringue )

Remouer sans cesse a feu doux pour faire épaissir la préparation

Une fois le mélange épaissi sortir la casserole du feu et incorporer le beurre

Répartir le tout sur la pate dans un plat a tarte

Cuire 20 min a 200°

### Meuringue

Battre les blancs en neige en incorporant le sucre et le sel.

Disposer les blancs battus sur la tarte cuite en formant des pointes (c'est plus joli!) et mettre au [four](https://www.marmiton.org/shopping/envie-d-un-four-encastrable-au-top-voici-les-6-meilleurs-fours-avec-pyrolyse-s4008147.html?utm_source=ustensiles-recettes)
sur position grill pendant quelques minutes pour colorer les pointes.
