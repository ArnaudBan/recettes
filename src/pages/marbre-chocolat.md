---
title: Cake Marbré au Chocolat
mealType: Gouter
layout: ../layouts/MainLayout.astro
---
## Ingrédiens

- 240g de farine
- 150g de sucre
- 1 sachet de levure
- 1 pincé de sel
- 3 oeufs
- 75g de beurre fondu
- 1 verre de lait
- 200g de chocolat

## Recette

Blanchir les oeufs avec le sucre

Ajouter lentement le beurre afin de ne pas faire tomber les oeufs

Incorporer la farine et la levure

Ajouter le lait

Verser la moitié de la pâte dans un moule à cake beurré et fariné

Faire fondre le chocolat à feu très doux

Ajouter le chocolat fondu au restant de la pâte 

Verser la pâte au chocolat sur la pâte déjà présente dans le moule

A l'aide d'un couteau, mélanger rapidement les 2 pâtes en faisant un mouvement de gauche à droite et de droite à gauche dans le moule.

Enfourner à 160°C (thermostat 5-6) jusqu'à ce que la pointe du couteau ressorte sèche
