---
title: Fondant au chocolat
mealType: Dessert
layout: ../layouts/MainLayout.astro
---
## Ingrédiens

- 100g de farine
- 150g de sucre
- 1 pincé de sel
- 4 oeufs
- 150g de beurre fondu
- 200g de chocolat

## Recette

Faire fondre le chocolat et le beurre

Pendant ce temps mélanger la farine, le sucre, le sel et les œufs

Quand le chocolat et le beurre sont fondu les mélanger au mélange sec

Enfourner à 150°C pendant 15 min
